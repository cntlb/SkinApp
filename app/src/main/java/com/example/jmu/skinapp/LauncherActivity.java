package com.example.jmu.skinapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.nurseryrhyme.skin.SkinManager;

public class LauncherActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SkinManager skinManager = SkinManager.getInstance();
        skinManager.init(this);
        skinManager.load(Environment.getExternalStorageDirectory() + "/bluetooth/skin-spring-debug.apk",
                new SkinManager.OnLoadSkinListener() {
            @Override
            public void onSuccess() {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }

            @Override
            public void onFailure() {
                Toast.makeText(LauncherActivity.this, "Skin load onFailure", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
