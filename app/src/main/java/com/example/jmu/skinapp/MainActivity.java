package com.example.jmu.skinapp;

import android.content.Intent;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nurseryrhyme.skin.Attr;
import com.nurseryrhyme.skin.AttrFactory;
import com.nurseryrhyme.skin.InflaterHelper;
import com.nurseryrhyme.skin.SkinChangeObserver;
import com.nurseryrhyme.skin.SkinLayoutInflater;
import com.nurseryrhyme.skin.SkinManager;
import com.nurseryrhyme.skin.attr.BackgroundAttr;
import com.nurseryrhyme.skin.statusbar.StatusBarBackground;


public class MainActivity extends AppCompatActivity implements SkinChangeObserver {

    private static final String TAG = "MainActivity";
    public static final String SKIN_SPRING = Environment.getExternalStorageDirectory() + "/bluetooth/skin-spring-debug.apk";
    private SkinLayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inflater = new SkinLayoutInflater(this);
        super.onCreate(savedInstanceState);
        updateStatusBarColor();
        setContentView(inflater.inflate(R.layout.activity_main, null));
        Toolbar toolbar = findViewById(R.id.toolbar);
//        inflater.addSkinAttr(toolbar, Attr.ATTR_BACKGROUND, R.color.colorPrimary);
        setSupportActionBar(toolbar);
        SkinManager.attach(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SkinManager.detach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.skin_select_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.skin_default:
                SkinManager.getInstance().defaultSkin();
                return true;
            case R.id.skin_spring:
                SkinManager.getInstance().load(SKIN_SPRING, null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public SkinLayoutInflater getLayoutInflater() {
        return inflater;
    }

    @Override
    public void onSkinChanged() {
        updateStatusBarColor();
        inflater.apply();
    }

    private void updateStatusBarColor() {
        int color = SkinManager.getInstance().getColor(R.color.colorPrimaryDark);
        StatusBarBackground.setColor(this, color);
    }

    public void nextActivity(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
