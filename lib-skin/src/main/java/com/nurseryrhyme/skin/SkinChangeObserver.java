package com.nurseryrhyme.skin;

/**
 * @author Linbing Tang
 * @since 18-05-11
 */
public interface SkinChangeObserver {
    /**
     * 皮肤更改
     */
    void onSkinChanged();
}
