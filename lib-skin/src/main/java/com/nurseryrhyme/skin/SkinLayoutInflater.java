package com.nurseryrhyme.skin;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.AnyRes;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurseryrhyme.skin.attr.SkinAttr;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SkinLayoutInflater extends LayoutInflater {
    private static final String[] sClassPrefixList = {
            "android.widget.",
            "android.webkit.",
            "android.app."
    };
    private List<SkinInfo> skinInfos = new ArrayList<>();

    public SkinLayoutInflater(Context context) {
        super(context);
        setFactory2(new Factory2());
    }

    public SkinLayoutInflater(LayoutInflater original, Context newContext) {
        super(original, newContext);
    }

    @Override
    public LayoutInflater cloneInContext(Context newContext) {
        return new SkinLayoutInflater(this, newContext);
    }

    @Override
    protected View onCreateView(String name, AttributeSet attrs) throws ClassNotFoundException {
        View view = null;
        for (String prefix : sClassPrefixList) {
            try {
                view = createView(name, prefix, attrs);
                if (view != null) {
                    break;
                }
            } catch (ClassNotFoundException e) {
                // In this case we want to let the base class take a crack
                // at it.
            }
        }
        if (view == null) {
            view = super.onCreateView(name, attrs);
        }

        parseAttributeSet(view, attrs);
        return view;
    }

    public void addSkinAttrs(View view, SkinAttr... attrs) {
        skinInfos.add(new SkinInfo(view, new HashSet<>(Arrays.asList(attrs))));
    }

    public SkinLayoutInflater addSkinAttr(View view, String attrName, @AnyRes int id) {
        SkinInfo info = null;
        for (SkinInfo si : skinInfos) {
            if (si.getView() == view) {
                info = si;
                break;
            }
        }

        if (info == null) {
            info = new SkinInfo(view);
            skinInfos.add(info);
        }
        if (info.getSkinAttrs() == null) {
            info.setSkinAttrs(new HashSet<>());
        }

        SkinAttr skinAttr = AttrFactory.get(view.getResources(), attrName, id);
        info.getSkinAttrs().add(skinAttr);
        skinAttr.apply(view);
        return this;
    }

    public void apply() {
        for (SkinInfo info : skinInfos) {
            info.apply();
        }
    }

    public void clear() {
        for (SkinInfo info : skinInfos) {
            info.clear();
        }
    }

    private void parseAttributeSet(View view, AttributeSet attrs) {
        if (view == null) {
            return;
        }

        if (!attrs.getAttributeBooleanValue(SkinConfig.NAMESPACE, SkinConfig.ENABLE, false)) {
            return;
        }

        Set<SkinAttr> attrSet = new HashSet<>();
        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            String attributeName = attrs.getAttributeName(i);
            if (!AttrFactory.isAttributeSupported(attributeName)) {
                continue;
            }

            String attributeValue = attrs.getAttributeValue(SkinConfig.NAMESPACE, attributeName);
            if (TextUtils.isEmpty(attributeValue)) {
                continue;
            }

            if (!attributeValue.startsWith("@")) {
                continue;
            }

            try {
                int id = Integer.parseInt(attributeValue.substring(1));
                Resources resources = view.getResources();
                String resourceEntryName = resources.getResourceEntryName(id);
                String resourceTypeName = resources.getResourceTypeName(id);
                SkinAttr attr = AttrFactory.get(attributeName, id, resourceEntryName, resourceTypeName);
                if (attr != null) {
                    attrSet.add(attr);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (attrSet.isEmpty()) {
            return;
        }

        SkinInfo info = new SkinInfo(view, attrSet);
        skinInfos.add(info);
        info.apply();
    }

    private class Factory2 implements LayoutInflater.Factory2 {

        @Override
        public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
            if (name.indexOf('.') == -1) {
                return null;
            }

            // 自定义控件部分(support包也在此)
            View view = null;
            try {
                view = createView(name, null, attrs);
                parseAttributeSet(view, attrs);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return view;
        }

        @Override
        public View onCreateView(String name, Context context, AttributeSet attrs) {
            return null;
        }
    }
}
