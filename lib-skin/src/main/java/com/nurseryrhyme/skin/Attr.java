package com.nurseryrhyme.skin;

import android.view.View;

/**
 * @author Linbing Tang
 * @since 18-05-11
 */
public interface Attr {
    /**
     * textColor attribute. e.g: skin:textColor="@color/colorPrimary"
     */
    String ATTR_TEXT_COLOR = "textColor";

    /**
     * background attribute. e.g: skin:background="@color/colorPrimary"
     */
    String ATTR_BACKGROUND = "background";

    /**
     * "color" type of attribute value: @color/white
     */
    String TYPE_COLOR = "color";

    /**
     * "drawable" type of attribute value: @drawable/ic_launcher
     */
    String TYPE_DRAWABLE = "drawable";

    /**
     * 应用皮肤
     */
    void apply(View view);
}
