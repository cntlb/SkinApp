package com.nurseryrhyme.skin;

import android.content.res.Resources;

import com.nurseryrhyme.skin.attr.BackgroundAttr;
import com.nurseryrhyme.skin.attr.SkinAttr;
import com.nurseryrhyme.skin.attr.TextColorAttr;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Linbing Tang
 * @since 18-05-11
 */
public class AttrFactory {
    public static final String TEXT_COLOR = "textColor";
    public static final String BACKGROUND = "background";

    public static final Map<String, Class<? extends SkinAttr>> CLASS_MAP;

    static {
        CLASS_MAP = new HashMap<>();
        CLASS_MAP.put(TEXT_COLOR, TextColorAttr.class);
        CLASS_MAP.put(BACKGROUND, BackgroundAttr.class);
    }

    /**
     * 注册属性, {@link SkinAttr}子类必须拥有无参数构造方法
     * @param attrName 属性名, 如 {@link Attr#ATTR_BACKGROUND background}, {@link Attr#ATTR_TEXT_COLOR textColor}
     */
    public static void registerSkinAttr(String attrName, Class<? extends SkinAttr> clazz) {
        CLASS_MAP.put(attrName, clazz);
    }

    /**
     * 以 {@code skin:textColor="@color/black"}为例说明参数
     *
     * @param attrName      属性名:如 {@link Attr#ATTR_BACKGROUND background}, {@link Attr#ATTR_TEXT_COLOR textColor},
     *                      例子中为:textColor
     * @param id            资源id, 也就是R.color.black
     * @param attrValueName 属性值名称:black
     * @param attrValueType 属性值类型:color
     * @return SkinAttr
     */
    public static SkinAttr get(String attrName, int id, String attrValueName, String attrValueType) {
        Class<? extends SkinAttr> clazz = CLASS_MAP.get(attrName);
        if (clazz == null) {
            return null;
        }
        SkinAttr attr;
        try {
            attr = clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        attr.attrName = attrName;
        attr.attrValueRefId = id;
        attr.attrValueRefName = attrValueName;
        attr.attrValueTypeName = attrValueType;
        return attr;
    }

    /**
     * @param attrName      属性名:如 {@link Attr#ATTR_BACKGROUND background}, {@link Attr#ATTR_TEXT_COLOR textColor}
     * @param id    resource id
     */
    public static SkinAttr get(Resources resources, String attrName, int id) {
        String entryName = resources.getResourceEntryName(id);
        String typeName = resources.getResourceTypeName(id);
        return get(attrName, id, entryName, typeName);
    }

    /**
     * 换肤属性是否支持
     *
     * @param attrName 属性名
     * @return true支持, 否则false
     */
    public static boolean isAttributeSupported(String attrName) {
        return CLASS_MAP.containsKey(attrName);
    }
}
