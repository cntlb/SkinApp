package com.nurseryrhyme.skin;

import android.view.LayoutInflater;

import java.lang.reflect.Field;

/**
 * @author Linbing Tang
 * @since 18-05-11 17:33
 */
public class InflaterHelper {

    private InflaterHelper() {
    }

    public static void set(LayoutInflater inflater, LayoutInflater.Factory2 factory2) {
        try {
            Field mFactorySet = LayoutInflater.class.getDeclaredField("mFactorySet");
            mFactorySet.setAccessible(true);
            mFactorySet.set(inflater, false);
            inflater.setFactory2(factory2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
