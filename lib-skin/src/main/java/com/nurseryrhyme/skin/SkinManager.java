package com.nurseryrhyme.skin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.ColorRes;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Linbing Tang
 * @since 18-05-11
 */
@SuppressLint("StaticFieldLeak")
public class SkinManager {

    private Context context;
    private static SkinManager sSkinManager;
    private Resources resources;
    private String skinPackageName;
    private String skinPath;
    private boolean isDefaultSkin;
    private boolean initialized;
    private List<WeakReference<SkinChangeObserver>> observables;

    public static synchronized SkinManager getInstance() {
        if (sSkinManager == null) {
            sSkinManager = new SkinManager();
        }
        return sSkinManager;
    }

    public void init(Context context) {
        if (initialized) {
            return;
        }
        this.context = context.getApplicationContext();
        observables = new ArrayList<>();
        initialized = true;
    }

    public static void attach(SkinChangeObserver observer) {
        getInstance().observables.add(new WeakReference<>(observer));
    }

    public static void detach(SkinChangeObserver observer) {
        Iterator<WeakReference<SkinChangeObserver>> it = getInstance().observables.iterator();
        while (it.hasNext()) {
            WeakReference<SkinChangeObserver> reference = it.next();
            if (reference == null || reference.get() == null) {
                it.remove();
                continue;
            }

            if (reference.get() == observer) {
                it.remove();
            }
        }
    }

    public void notifySkinChanged() {
        Iterator<WeakReference<SkinChangeObserver>> it = observables.iterator();
        while (it.hasNext()) {
            WeakReference<SkinChangeObserver> reference = it.next();
            if (reference == null || reference.get() == null) {
                it.remove();
                continue;
            }
            reference.get().onSkinChanged();
        }
    }


    /**
     * 加载sdcard皮肤
     */
    public void load(String skinPackagePath, OnLoadSkinListener listener) {
        skinPath = skinPackagePath;
        final OnLoadSkinListener onLoadSkinListener;
        if (listener == null) {
            onLoadSkinListener = new OnLoadSkinListener() {
            };
        } else {
            onLoadSkinListener = listener;
        }
        new AsyncTask<String, Void, Resources>() {
            @Override
            protected Resources doInBackground(String... params) {
                if (params.length != 1) {
                    return null;
                }
                try {
                    String skinPkgPath = params[0];
                    Log.i("loadSkin", skinPkgPath);
                    File file = new File(skinPkgPath);
                    if (!file.exists()) {
                        return null;
                    }

                    PackageManager mPm = context.getPackageManager();
                    PackageInfo mInfo = mPm.getPackageArchiveInfo(skinPkgPath, PackageManager.GET_ACTIVITIES);
                    skinPackageName = mInfo.packageName;

                    AssetManager assetManager = AssetManager.class.newInstance();
                    Method addAssetPath = assetManager.getClass().getMethod("addAssetPath", String.class);
                    addAssetPath.invoke(assetManager, skinPkgPath);

                    Resources superRes = context.getResources();
                    Resources skinResource = new Resources(assetManager, superRes.getDisplayMetrics(), superRes.getConfiguration());

                    skinPath = skinPkgPath;
                    isDefaultSkin = false;
                    return skinResource;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Resources result) {
                resources = result;
                if (resources != null) {
                    onLoadSkinListener.onSuccess();
                    notifySkinChanged();
                } else {
                    isDefaultSkin = true;
                    onLoadSkinListener.onFailure();
                }
            }
        }.execute(skinPackagePath);
    }

    /**
     * 使用默认皮肤
     */
    public void defaultSkin() {
        isDefaultSkin = true;
        notifySkinChanged();
    }

    public int getColor(@ColorRes int id) {
        int originColor = context.getResources().getColor(id);
        if (resources == null || isDefaultSkin) {
            return originColor;
        }

        try {
            String name = context.getResources().getResourceEntryName(id);
            int identifier = resources.getIdentifier(name, Attr.TYPE_COLOR, skinPackageName);
            return resources.getColor(identifier);
        } catch (Resources.NotFoundException e) {
            return originColor;
        }
    }

    public ColorStateList getColorStateList(int id) {
        ColorStateList origin = context.getResources().getColorStateList(id);
        if (resources == null || isDefaultSkin) {
            return origin;
        }

        try {
            String name = context.getResources().getResourceEntryName(id);
            int identifier = resources.getIdentifier(name, Attr.TYPE_DRAWABLE, skinPackageName);
            if (android.os.Build.VERSION.SDK_INT <= 22) {
                return resources.getColorStateList(identifier);
            } else {
                return resources.getColorStateList(identifier, null);
            }
        } catch (Resources.NotFoundException e) {
            return origin;
        }
    }


    public Drawable getDrawable(int id) {
        Drawable originDrawable = context.getResources().getDrawable(id);
        if (resources == null || isDefaultSkin) {
            return originDrawable;
        }
        String resName = context.getResources().getResourceEntryName(id);

        int trueResId = resources.getIdentifier(resName, Attr.TYPE_DRAWABLE, skinPackageName);

        Drawable trueDrawable;
        try {
            if (android.os.Build.VERSION.SDK_INT < 22) {
                trueDrawable = resources.getDrawable(trueResId);
            } else {
                trueDrawable = resources.getDrawable(trueResId, null);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            trueDrawable = originDrawable;
        }

        return trueDrawable;
    }

    /**
     * 加载指定资源颜色drawable,转化为ColorStateList，保证selector类型的Color也能被转换。</br>
     * 无皮肤包资源返回默认主题颜色
     */
    public ColorStateList convertToColorStateList(int resId) {
        boolean isExtendSkin = true;
        if (resources == null || isDefaultSkin) {
            isExtendSkin = false;
        }

        String resName = context.getResources().getResourceEntryName(resId);
        try {
            if (isExtendSkin) {
                int trueResId = resources.getIdentifier(resName, Attr.TYPE_COLOR, skinPackageName);
                ColorStateList trueColorList = null;
                if (trueResId == 0) { // 如果皮肤包没有复写该资源，但是需要判断是否是ColorStateList
                    return context.getResources().getColorStateList(resId);
                } else {
                    trueColorList = resources.getColorStateList(trueResId);
                    return trueColorList;
                }
            } else {
                return context.getResources().getColorStateList(resId);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        int[][] states = new int[1][1];
        return new ColorStateList(states, new int[]{context.getResources().getColor(resId)});
    }


    public interface OnLoadSkinListener {
        /**
         * 加载成功
         */
        default void onSuccess() {
        }

        /**
         * 加载失败
         */
        default void onFailure() {
        }
    }
}
