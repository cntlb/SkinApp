package com.nurseryrhyme.skin;

import android.view.View;

import com.nurseryrhyme.skin.attr.SkinAttr;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Linbing Tang
 * @since 18-05-11
 */
class SkinInfo {
    private View view;
    private Set<SkinAttr> attrs;

    public SkinInfo(View view) {
        this.view = view;
    }

    public SkinInfo(View view, Set<? extends SkinAttr> attrs) {
        this.view = view;
        this.attrs = new HashSet<>(attrs);
    }

    public void apply() {
        for (SkinAttr attr : attrs) {
            attr.apply(view);
        }
    }

    public void clear(){
        attrs.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SkinInfo info = (SkinInfo) o;

        return view.equals(info.view);
    }

    @Override
    public int hashCode() {
        return view.hashCode();
    }

    public View getView() {
        return view;
    }

    public SkinInfo setView(View view) {
        this.view = view;
        return this;
    }

    public Set<SkinAttr> getSkinAttrs() {
        return attrs;
    }

    public void setSkinAttrs(Set<? extends SkinAttr> attrs) {
        this.attrs = new HashSet<>(attrs);
    }
}
