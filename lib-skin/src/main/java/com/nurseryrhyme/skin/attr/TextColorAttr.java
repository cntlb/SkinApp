package com.nurseryrhyme.skin.attr;

import android.view.View;
import android.widget.TextView;

import com.nurseryrhyme.skin.SkinManager;

/**
 *
 * @author Linbing Tang
 * @since 18-05-11
 */
public class TextColorAttr extends SkinAttr {

    @Override
    public void apply(View view) {
        if (!(view instanceof TextView)) {
            return;
        }

        if (TYPE_COLOR.equals(attrValueTypeName)) {
            ((TextView) view).setTextColor(SkinManager.getInstance().getColor(attrValueRefId));
            return;
        }

        if(TYPE_DRAWABLE.equals(attrValueTypeName)){
            ((TextView) view).setTextColor(SkinManager.getInstance().getColorStateList(attrValueRefId));
        }
    }
}
