package com.nurseryrhyme.skin.attr;

import com.nurseryrhyme.skin.Attr;

/**
 *
 * @author Linbing Tang
 * @since 18-05-11
 */
public abstract class SkinAttr implements Attr {

    /**
     * 属性名, 例如: background、textSize、textColor
     */
    public String attrName;

    /**
     * 属性值的引用id
     */
    public int attrValueRefId;

    /**
     * 资源的名字, 例如 [app_exit_btn_background]
     */
    public String attrValueRefName;

    /**
     * type of the value , such as color or drawable
     */
    public String attrValueTypeName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SkinAttr)) return false;

        SkinAttr attr = (SkinAttr) o;

        if (attrValueRefId != attr.attrValueRefId) return false;
        if (!attrName.equals(attr.attrName)) return false;
        if (!attrValueRefName.equals(attr.attrValueRefName)) return false;
        return attrValueTypeName.equals(attr.attrValueTypeName);
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + attrValueRefId;
        result = 31 * result + attrValueRefName.hashCode();
        result = 31 * result + attrValueTypeName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SkinAttr \n[\nattrName=" + attrName + ", \n"
                + "attrValueRefId=" + attrValueRefId + ", \n"
                + "attrValueRefName=" + attrValueRefName + ", \n"
                + "attrValueTypeName=" + attrValueTypeName
                + "\n]";
    }
}
