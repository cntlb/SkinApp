package com.nurseryrhyme.skin.attr;

import android.view.View;

import com.nurseryrhyme.skin.SkinManager;

/**
 *
 *
 * @author Linbing Tang
 * @since 18-05-11
 */
public class BackgroundAttr extends SkinAttr {

    @Override
    public void apply(View view) {
        if (TYPE_COLOR.equals(attrValueTypeName)) {
            view.setBackgroundColor(SkinManager.getInstance().getColor(attrValueRefId));
            return;
        }

        if (TYPE_DRAWABLE.equals(attrValueTypeName)) {
            view.setBackgroundDrawable(SkinManager.getInstance().getDrawable(attrValueRefId));
        }
    }
}
