package com.nurseryrhyme.skin;

/**
 *
 * @author Linbing Tang
 * @since 18-05-11
 */
public class SkinConfig {
    /**
     * 换肤namespace
     */
    public static final String NAMESPACE = "http://schemas.android.com/apk/skin";
    /**
     * 控件支持换肤
     * <pre>
     *     xmlns:skin="http://schemas.android.com/android/skin"
     *     &lt;TextView
     *      skin:enable="true"
     *     />
     * </pre>
     */
    public static final String ENABLE = "enable";


    private SkinConfig() {
    }
}
